
//********************************************************************
//File:         PalindromeDriver.java       
//Author:       Kalvin Dewey
//Date:         10/20/17
//Course:       CPS100
//
//Problem Statement:
//Create a modified version of the PalindromeTester program so
//that the spaces, punctuation, and changes in uppercase and low-
//ercase are not considered when determining whether a string is a
//palindrome.
//
//Inputs: A potential palindrome
//Outputs:  whether the input is a palindrome
// 
//*******************************************************************
import java.util.Scanner;


public class PalindromeDriver
{
  public static void main(String[] args)
  {
    String str, another = "y";
    Scanner scan = new Scanner(System.in);
    PalindromeTester tester = new PalindromeTester();

    while (another.equalsIgnoreCase("y")) // allows y or Y
    {
      System.out.println("Enter a potential palindrome:");
      str = scan.nextLine();

      if (tester.isPalindrome(str))
        System.out.println("That string IS a palindrome.");
      else
        System.out.println("That string is NOT a palindrome.");

      System.out.println();
      System.out.print("Test another palindrome (y/n)? ");
      another = scan.nextLine();

      System.out.println();
    }
    scan.close();
  }

}
