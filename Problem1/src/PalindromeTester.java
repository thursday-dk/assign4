
public class PalindromeTester
{
  //-----------------------------------------------------------------
  //  Tests strings to see if they are palindromes.
  //-----------------------------------------------------------------
  public PalindromeTester()
  {

  }

  public boolean isPalindrome(String inputStr)
  {
    String unFlipStr, flipStr;
    int numChars;
    {
      unFlipStr = "";
      flipStr = "";
      numChars = 0;

      while (numChars <= inputStr.length() - 1)
      {
        if (Character.isLetter(inputStr.charAt(numChars)))
          unFlipStr += inputStr.charAt(numChars);
        numChars++;
      }
      numChars = unFlipStr.length() - 1;
      while (numChars >= 0)
      {
        flipStr += unFlipStr.charAt(numChars);
        numChars--;
      }

      return unFlipStr.equalsIgnoreCase(flipStr);
    }
  }
}
