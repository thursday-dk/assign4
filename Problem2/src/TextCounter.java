import java.util.ArrayList;


public class TextCounter
{
  private ArrayList<Character> chars = new ArrayList<Character>();
  private ArrayList<Integer> ints = new ArrayList<Integer>();

  public TextCounter()
  {

  }

  public void addChar(char add)
  {
    if (chars.contains(add))
    {
      int index = chars.indexOf(add);
      ints.set(index, ints.get(index) + 1);
    }
    else
    {
      chars.add(add);
      ints.add(1);
    }
  }

  public void printArrays()
  {
    int index = 0;
    while (index <= chars.size() - 1)
    {
      System.out.println(chars.get(index) + ":" + ints.get(index));
      index++;
    }
  }
}
