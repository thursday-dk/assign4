
//********************************************************************
//File:         TextDriver.java       
//Author:       Kalvin Dewey
//Date:         10/22/17
//Course:       CPS100
//
//Problem Statement:
//Design and implement a program that counts the number of
//punctuation marks in a text input file. Produce a table that
//shows how many times each symbol occurred.
//
//Inputs: A text file
//Outputs: a table that shows how many times each symbol occurred.
// 
//********************************************************************

import java.util.Scanner;
import java.io.*;


public class TextDriver
{

  public static void main(String[] args) throws IOException
  {
    TextCounter counter = new TextCounter();
    String lineToScan;
    Scanner fileScan;
    fileScan = new Scanner(new File("frankenstein.txt"));
    
    while (fileScan.hasNext())
    {
      lineToScan = fileScan.nextLine();
      int numChars = 0;
      
      while (numChars <= lineToScan.length() - 1)
      {
        if (!Character.isLetterOrDigit(lineToScan.charAt(numChars)))
          counter.addChar(lineToScan.charAt(numChars));
        
        numChars++;
      }
    }
    
    counter.printArrays();
    fileScan.close();
  }

}
